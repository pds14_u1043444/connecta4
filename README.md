Connecta4
=============
Programa de simulació del joc Connecta4. Implementa diversos
algorismes IA com el Mini-Max i l'Alfa-beta

Autors
=======
- Jordi Basacoma
- Santi Pérez

Compilació
=======================


Execució
=======================


Comandes del git
================
- **git pull = git fetch + git merge -->** Consulta què hi ha al repositori
                  i si hi ha algun canvi respecte el que tens, t'ho intenta
				  afegir.
- **git stash  --> ** Guarda en una "pila" local els teus canvis que no vols perdre.
- **git stash show  --> ** T'ensenya què hi tens a la teva "pila" local.
- **git stash list  --> ** Et llista el que tens a la teva "pila" local.
- **git stash pop  --> ** Torna a posar els teus canvis al codi que tens actualment.
- **git diff *nom_fitxer* -->** Et mostra les diferències que hi ha després d'un merge.
- **git add -p  --> ** Permet afegir només una part dels canvis.
- **git add *nom_fitxer*  --> ** Només afegeix el fitxer que li passes com a paràmetre.
- **git reset HEAD *nom_fitxer*  --> ** Per treure un fitxer de l'stage després d'haver fet un add.
- **git status  --> ** Et mostra què et falta afegir, què hi ha modificat, què és nou...
- **git commit -m** "Missatge relacionat amb el que s'ha canviat."
- **git push *origin master* ** (*opcional*: branca_orígen branca_destí) **-->** Penja els arxius del commit al servidor.
- **git fetch  --> ** Només compara dues branques i si fas diff et diu què hi ha de diferent.
- **git merge  --> ** Intenta fusionar dues branques.
- **git rebase  --> ** Una mica perillosa per inexperts, millor no la fem servir...
- **git revert *codi_commit*  --> ** Per desfer algun commit que s'hagi pujat al repositori i estigui malament.
                             Un cop executat, s'ha de fer commit del revert cap al repositori. codi_commit
                             és el que hi ha al bitbucket.
- **git fetch --all --> ** descargues els ultims canvis, sense tractar de fusionar amb el teu codi.
- **git reset --hard --> ** descarga tots els arxius de la branca principal, i el --hard canvia tots els arxius del teu PC
                            perque coincideixin amb l'origin master

