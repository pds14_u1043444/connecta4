/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//package connecta4;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Usuari
 */
public class Player2 {
    private Tauler meutaulell;
    Player2(Tauler entrada){
        meutaulell = entrada;
    }
    public int[] tirada(){
        /*
        int x,y;
        //busco una posicio buida
        for(int i=0;i<meutaulell.getX();i++){
            for(int j=0;j<meutaulell.getY();j++){
                if (meutaulell.getpos(i,j) == 0){
                    return new int[]{i,j}; 
                }
            }
        }
        
        //Un retorn per defecte
        return new int[]{1,1};
        */
        //return this.triaTirada(this.possiblesTirades());

        Integer columna, fila;
        Scanner s = new Scanner(System.in);
        System.out.println("Entra columna");
        columna = s.nextInt();
        System.out.println("Entra fila");
        fila = s.nextInt();
        return new int[]{columna-1,fila-1};
    }

    /**
     *
     * @return
     */
    private ArrayList<int[]> possiblesTirades() {
        ArrayList<int[]> possiblesTirades = new ArrayList<int[]>();
        for(int i=0;i<this.meutaulell.getX();i++){
            int j=0; boolean trobat = false;
            while(j<this.meutaulell.getY() && !trobat) {
                if(this.meutaulell.getpos(i,j) == 0) trobat=true;
                else j++;
            }
            if(trobat) possiblesTirades.add(new int[]{i,j});
        }
        return possiblesTirades;
    }

    private int[] triaTirada(ArrayList<int[]> possiblesTirades) {
        return possiblesTirades.get((new Random()).nextInt(possiblesTirades.size()));
    }
}
