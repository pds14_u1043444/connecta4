/**
 * @class Player1
 * @brief Tria la millor tirada pel jugador.
 * @details Tria la millor tirada fent servir dos metodes. En primer lloc es comprova si en alguna de les tirades
 *          inmediates s'obte alguna de puntuacio maxima, cosa que voldra dir que s'ha de realitzar aquesta. De no
 *          ser aixi, el que fa es llencar un algorisme de cerca (minimax o alfabeta) per trobar la millor tirada
 *          possible depenent d'aquest algorisme.
 * @author Jordi Basacoma (u1911716) i Santi Perez (u1043444)
 * @version 2015.03.25
 */
//package connecta4;

import java.util.ArrayList;
import java.util.Iterator;


public class Player1 {
    /**
     * Tauler del Player1.
     */
    private Tauler meutaulell;

    /**
     * Inicialitza el Player1 amb el tauler passat per parametre.
     *
     * @param entrada Tauler que rep el Player1
     */
    Player1(Tauler entrada){
        meutaulell = entrada;
    }

    /**
     * S'encarrega de triar la millor tirada pel Player1.
     *
     * @return Tirada que realitzara el Player1 al tauler grafic.
     */
    public int[] tirada() {
        // Conversio del tauler inicial al nostre format TaulerMatriu
        TaulerMatriu taulerInicial = new TaulerMatriu(meutaulell);
        Algorisme minimax = new Algorisme(taulerInicial, 7, 0, false);


        // Comprovar al primer nivell si s'ha de tapar o guanyar directament
        ArrayList<TaulerMatriu> fills = minimax.generaMoviments();
        Iterator<TaulerMatriu> iterator = fills.iterator();
        boolean trobat = false;
        TaulerMatriu taulerMatriu = null;
        while(iterator.hasNext() && !trobat) {
            taulerMatriu = iterator.next();
            trobat = (new Algorisme(taulerMatriu,0,0,false)).heuristica() >= 2000;

        }

        // Si estem al primer nivell i hi ha un maxim, cal tirar alla
        if(trobat) return taulerMatriu.getPosicioModificada();
        // No hem trobat cap maxim, cal executar el minimax
        ArrayList<Algorisme> algorismes = new ArrayList<Algorisme>();
        minimax.executaMinimax(algorismes);

        // Si hem tingut empats de jugades guanyadores, triarem la que tiri mes cap al mig
        if(algorismes.size()>1) return algorismes.get(algorismes.size()/2).getTaulerMillor().getPosicioModificada();

        // No hem tingut cap empat, per tant triem la unica jugada que hem trobat
        return algorismes.get(0).getTaulerMillor().getPosicioModificada();


//        // Conversio del tauler inicial al nostre format TaulerMatriu
//        TaulerMatriu taulerInicial = new TaulerMatriu(meutaulell);
//        Algorisme alfabeta = new Algorisme(taulerInicial, 7, 0, false, Integer.MIN_VALUE, Integer.MAX_VALUE);
//
//
//        // Comprovar al primer nivell si s'ha de tapar o guanyar directament
//        ArrayList<TaulerMatriu> fills = alfabeta.generaMoviments();
//        Iterator<TaulerMatriu> iterator = fills.iterator();
//        boolean trobat = false;
//        TaulerMatriu taulerMatriu = null;
//        while(iterator.hasNext() && !trobat) {
//            taulerMatriu = iterator.next();
//            trobat = (new Algorisme(taulerMatriu,0,0,false)).heuristica() >= 2000;
//
//        }
//
//        // Si estem al primer nivell i hi ha un maxim, cal tirar alla
//        if(trobat) return taulerMatriu.getPosicioModificada();
//
//        // No hem trobat cap maxim, cal executar l'alfabeta
//        ArrayList<Algorisme> algorismes = new ArrayList<Algorisme>();
//        alfabeta.executaAlfaBeta(algorismes);
//
//        // Si hem tingut empats de jugades guanyadores, triarem la que tiri mes cap al mig
//        if(algorismes.size()>1) return algorismes.get(algorismes.size()/2).getTaulerMillor().getPosicioModificada();
//
//        // No hem tingut cap empat, per tant triem la unica jugada que hem trobat
//        return algorismes.get(0).getTaulerMillor().getPosicioModificada();
    }

    /**
     * Classe que representa els dos algorismes (minimax i alfabeta)
     * i els nodes de l'arbre de cerca.
     */
    private class Algorisme {
        /**
         * Nivell maxim de profunditat de l'arbre de solucions
         */
        int maxNivell;
        /**
         * Nivell actual que s'esta explorant
         */
        int nivellActual;
        /**
         * Estat actual del tauler (seran els nodes de l'arbre)
         */
        TaulerMatriu taulerActual;
        /**
         * Millor tauler possible segons el Minimax
         */
        TaulerMatriu taulerMillor;
        /**
         * Possibles estats a partir del node (estat) actual
         */
        ArrayList<TaulerMatriu> fills;
        /**
         * Valor true = min; valor false = max
         */
        boolean min;
        /**
         * Valor de l'alfa.
         */
        int alfa;
        /**
         * Valor de la beta.
         */
        int beta;

        /**
         * Inicialitza un nou Algorisme amb el valor dels parametres.
         *
         * @param taulerActual Tauler sobre el que s'esta treballant.
         * @param maxNivell Maxim nivell d'exploracio.
         * @param nivellActual Nivell actual dins l'arbre del joc.
         * @param min Tipus de node (min o max).
         */
        private Algorisme(TaulerMatriu taulerActual, int maxNivell, int nivellActual, boolean min) {
            this.taulerActual = taulerActual;
            this.maxNivell = maxNivell;
            this.nivellActual = nivellActual;
            this.min = min;
        }

        /**
         * Inicialitza un nou Algorisme amb el valor dels parametres.
         *
         * @param taulerActual Tauler sobre el que s'esta treballant.
         * @param maxNivell Maxim nivell d'exploracio.
         * @param nivellActual Nivell actual dins l'arbre del joc.
         * @param min Tipus de node (min o max).
         * @param alfa Valor de l'alfa.
         * @param beta Valor de la beta.
         */
        private Algorisme(TaulerMatriu taulerActual, int maxNivell, int nivellActual, boolean min, int alfa, int beta) {
            this.taulerActual = taulerActual;
            this.maxNivell = maxNivell;
            this.nivellActual = nivellActual;
            this.min = min;
            this.alfa = alfa;
            this.beta = beta;
        }

        /**
         *
         * @return El millor tauler.
         */
        public TaulerMatriu getTaulerMillor() {
            return taulerMillor;
        }

        /**
         * Representa l'algorisme Minimax. A diferencia de l'original, el parametre
         * es diferent, sent en aquest cas el conjunt de les millors possibles jugades
         * que ha trobat, es a dir, si troba empats, els guarda.
         *
         * @param algorismes Contenidor que guardara les millors jugades.
         * @return La millor heuristica de totes les jugades possibles.
         */
        private int executaMinimax(ArrayList<Algorisme> algorismes) {
            if(prouProfund()) return heuristica();
            else {
                fills = generaMoviments();
                if(fills.isEmpty()) return heuristica();
                else {
                    int millor;
                    if(min) { // nivell(node) = MIN
                        millor = Integer.MAX_VALUE;
                        for(TaulerMatriu taulerM : fills) {
                            // valor = minimax(fill, nivell+1)
                            Algorisme fill = new Algorisme(taulerM, maxNivell, nivellActual+1, !min);
                            int valor = fill.executaMinimax(algorismes);
                            if(valor < millor) millor = valor;
                        }
                    }
                    else { // nivell(node) = MAX
                        millor = Integer.MIN_VALUE;
                        for(TaulerMatriu taulerM : fills) {
                            // valor = minimax(fill, nivell+1)
                            Algorisme fill = new Algorisme(taulerM, maxNivell, nivellActual+1, !min);
                            int valor = fill.executaMinimax(algorismes);
                            if(valor >= millor) {
                                if(fill.nivellActual == 1) {
                                    // Guardem nomes les millors jugades
                                    if(valor > millor && !algorismes.isEmpty()) algorismes.clear();
                                    fill.taulerMillor = fill.taulerActual;
                                    algorismes.add(fill);
                                }
                                millor = valor;
                            }
                        }
                    }
                    return millor;
                }
            }
        }

        /**
         * Representa l'algorisme AlfaBeta. A diferencia de l'original, el parametre
         * es diferent, sent en aquest cas el conjunt de les millors possibles jugades
         * que ha trobat, es a dir, si troba empats, els guarda.
         *
         * @param algorismes Contenidor que guardara les millors jugades.
         * @return La millor heuristica de totes les jugades possibles.
         */
        private int executaAlfaBeta(ArrayList<Algorisme> algorismes) {
            if(prouProfund()) return heuristica();
            else {
                fills = generaMoviments();
                if(fills.isEmpty()) return heuristica();
                else {
                    if(min) { // nivell(node) = MIN
                        Iterator<TaulerMatriu> iterator = fills.iterator();
                        boolean trobat = false;
                        while(iterator.hasNext() && !trobat) {
                            Algorisme fill = new Algorisme(iterator.next(),maxNivell,nivellActual+1,!min,alfa,beta);
                            int valor = fill.executaAlfaBeta(algorismes);
                            if(valor < beta) beta = valor;
                            trobat = alfa >= beta;
                        }
                        return beta;
                    }
                    else { // nivell(node) = MAX
                        Iterator<TaulerMatriu> iterator = fills.iterator();
                        boolean trobat = false;
                        while(iterator.hasNext() && !trobat) {
                            Algorisme fill = new Algorisme(iterator.next(),maxNivell,nivellActual+1,!min,alfa,beta);
                            int valor = fill.executaAlfaBeta(algorismes);
                            if(valor >= alfa) {
                                if(fill.nivellActual == 1) {
                                    // Guardem nomes les millors jugades
                                    if(valor > alfa && !algorismes.isEmpty()) algorismes.clear();
                                    fill.taulerMillor = fill.taulerActual;
                                    algorismes.add(fill);
                                }
                                alfa = valor;
                            }
                            trobat = alfa >= beta;
                        }
                        return alfa;
                    }
                }
            }
        }

        /**
         *
         * @return Cert si s'ha arribat a la profunditat indicada per maxNivell, fals altrament.
         */
        private boolean prouProfund() {
            return nivellActual >= maxNivell;
        }

        /**
         * Calcula l'heuristica de la tirada actual, delegant tasques a altres metodes.
         *
         * @return Heuristica de la tirada actual
         */
        private int heuristica() {
            // Columna de la tirada actual
            int columna = taulerActual.getPosicioModificada()[0];
            // Fila de la tirada actual
            int fila = taulerActual.getPosicioModificada()[1];

            // Si l'heuristica s'ha trobat a una profunditat de mes de 1, la dividim pel nivellActual
            if(nivellActual>1) return (heuristicaColumna(columna,fila) +
                                        heuristicaFila(columna,fila) +
                                        heuristicaDiagonal(columna,fila))
                                        / nivellActual;
            // Si es del primer nivell la retornem tota
            return heuristicaColumna(columna,fila) + heuristicaFila(columna,fila) + heuristicaDiagonal(columna,fila);
        }

        /**
         * Calcula l'heuristica de les columnes, delegant tasques a altres metodes.
         *
         * @param columna Columna de la tirada actual.
         * @param fila Fila de la tirada actual.
         * @return Heuristica de la fila.
         */
        private int heuristicaColumna(int columna, int fila) {
            // Color de la fitxa de la tirada actual
            int colorTirada = taulerActual.getPos(columna, fila);

            // Comprovem quantes fitxes del mateix color hi ha sota de la tirada
            // a mes de quedar-nos amb el seu color
            int [] colorPeces = comptaPecesColumna(columna,fila,colorTirada);
            int colorTrobades = colorPeces[0];
            int pecesTrobades = colorPeces[1];

            // Retornem l'heuristica segons el nombre de peces i el seu color
            return analitzaPecesTrobades(colorTirada, colorTrobades, pecesTrobades);
        }

        /**
         * Compta les peces del mateix color que hi ha a sota de la tirada actual,
         * retornant el seu color i quantes n'ha trobat.
         *
         * @param columna Columna de la tirada actual.
         * @param fila Fila de la tirada actual.
         * @param colorTirada Color de la tirada actual.
         * @return Color i numero de peces trobades.
         */
        private int [] comptaPecesColumna(int columna, int fila, int colorTirada) {
            // Guardem el color de les peces de sota
            int filaActual = fila-1;
            int colorTrobades = colorTirada;
            boolean pecesMateixColor = false;
            if(filaActual >= 0) {
                colorTrobades = taulerActual.getPos(columna, filaActual);
                // Per anar comprovant si la peca trobada te el mateix color
                pecesMateixColor = taulerActual.getPos(columna, filaActual) == colorTrobades;
            }

            // Recompte de peces
            int pecesTrobades = 0; boolean trobat = false;
            while(filaActual >= 0 && pecesMateixColor && !trobat) {
                if(taulerActual.getPos(columna, filaActual) != colorTrobades) pecesMateixColor = false;
                else {
                    pecesTrobades++;
                    // Hem guanyat per columna!
                    if(pecesTrobades == 3) trobat = true;
                        // Seguim mirant si queden peces del mateix color a la columna...
                    else filaActual--;
                }
            }
            // Retornem el color de les peces trobades i quantes s'ha trobat
            return new int[]{colorTrobades, pecesTrobades};
        }

        /**
         * Calcula el valor de l'heuristica a partir dels parametres passats.
         *
         * @param colorTirada Color de la fitxa de la tirada actual.
         * @param colorTrobades Color de les fitxes que hi ha a sota de la tirada actual.
         * @param pecesTrobades Nombre de peces trobades a sota de la tirada actual.
         * @return Valor de l'heuristica.
         */
        private int analitzaPecesTrobades(int colorTirada, int colorTrobades, int pecesTrobades) {
            // Fila de la tirada actual
            int fila = taulerActual.getPosicioModificada()[1];

            // No es pot fer connecta4, no cal seguir
            if(7-fila + pecesTrobades < 4) return 0;

            // A sota hi ha peces del color de la peca de la tirada
            if(colorTirada == colorTrobades) {
                // Menys de 4
                if(pecesTrobades < 4) return (int)Math.pow(10, pecesTrobades);// + ((4-pecesTrobades)*2);
                // Connecta4
                return 2500;
            }
            // A sota hi ha peces del contrari
            else {
                // El contrari te menys de 3 peces, ponderem
                if(pecesTrobades < 3) return (int)Math.pow(5, pecesTrobades);
                // el contrari te 3 peces o mes, cal defensar
                return 2000;
            }
        }

        /**
         * Calcula l'heuristica de les files, delegant tasques a altres metodes.
         *
         * @param columna Columna de la tirada actual.
         * @param fila Fila de la tirada actual.
         * @return Heuristica de la fila.
         */
        private int heuristicaFila(int columna, int fila) {
            // Generem el bloc per la fila on s'esta fent la tirada
            Bloc blocFila = blocFila(columna, fila);

            // Busquem els blocs que cal analitzar
            int [] blocsPerAnalitzar = triaBlocsFila(columna);

            // I en calculem l'heuristica
            return analitzaBlocs(blocsPerAnalitzar, blocFila);
        }

        /**
         * S'encarrega de triar el conjunt de subblocs que s'han
         * d'analitzar segons la columna de la tirada actual.
         *
         * @param columna Columna de la tirada actual.
         * @return Conjunt de subblocs que s'han d'analitzar.
         */
        public int [] triaBlocsFila(int columna) {
            // bloc 0: [0..3]
            // bloc 1: [1..4]
            // bloc 2: [2..5]
            // Com es pot veure, fem coincidir el nom del bloc {0,1,2}
            // amb l'index de la primera casella del bloc corresponent
            switch(columna) {
                case 0:  return new int[]{0};
                case 1:  return new int[]{0,1};
                case 2:  return new int[]{0,1,2};
                case 3:  return new int[]{0,1,2};
                case 4:  return new int[]{1,2};
                case 5:  return new int[]{2};
                default: return new int[]{};
            }
        }

        /**
         * Realitza el recompte de peces per cada jugador i els espais en blanc,
         * dels quals n'hi ha dos tipus (amb fitxa a sota i sense). Aquest recompte
         * el fa per cadascun dels subblocs que li indica el primer parametre, dins
         * el Bloc que representa el segon parametre. Un cop realitzat cada recompte,
         * calcula l'heuristica, retornant sempre la millor.
         *
         * @param blocs Subblocs a analitzar dins del Bloc.
         * @param bloc Bloc que conte els subblocs.
         * @return Heuristica del millor subbloc.
         */
        public int analitzaBlocs(int [] blocs, Bloc bloc) {
            // Cal comprovar si no hi ha blocs per analitzar
            if(blocs.length == 0) return 0;

            int heuristicaMillor = 0;

            int fila = bloc.getBloc().get(bloc.getModificada()).getColumna();
            int columna = bloc.getBloc().get(bloc.getModificada()).getFila();
            int color = taulerActual.getPos(fila,columna);

            // Analitzem els blocs connecta4 que diu el parametre blocs
            for(int numBloc : blocs) {

                // Iterem el bloc actual des de la posicio numBloc
                // 4 posicions (comptant l'actual)
                int j = numBloc; int pecesMeves, pecesContrari, buidaBona, buidaDolenta;
                pecesMeves = pecesContrari = buidaBona = buidaDolenta = 0;
                while(j < numBloc+4) {
                    int casellaActual = taulerActual.getPos(bloc.getBloc().get(j).getColumna(),
                            bloc.getBloc().get(j).getFila());

                    // Hi ha una peca del teu color
                    if (casellaActual == color) {
                        pecesMeves++;

                    }
                    // Hi ha una casella buida, l'increment de l'heuristica depen
                    else if (casellaActual == 0) {

                        // Comprovem que no estem a la fila 0 per evitar accessos fora de rang
                        if (fila != 0) {
                            int casellaDeSota = taulerActual.getPos(j, fila - 1);

                            // No hi ha cap peca a sota
                            if (casellaDeSota == 0) buidaDolenta++;
                                // Hi ha peca a sota
                            else buidaBona++;
                        }
                        // En cas que estem a la fila 0, no cal mirar si hi ha peca a sota o no
                        else buidaBona++;
                    }
                    // Hi ha una peca del contrari, l'heuristica ha de ser 0 i pleguem
                    else pecesContrari++;

                    j++;
                }

                int heuristica;
                //
                int valorPecesMeves = (int)Math.pow(10, pecesMeves);
                int valorPecesContrari = (int)Math.pow(5, pecesContrari);
                int valorBuides = (int)Math.pow(2, buidaBona) + buidaDolenta;

                // Calcul de l'heuristica
                // Defensa
                if(pecesContrari > 0) {
                    if(pecesContrari == 3) heuristica = 2000;
                    else heuristica = valorPecesContrari + valorBuides;
                }
                // Guanyar
                else {
                    if(pecesMeves == 4) {
                        heuristica = 2500;
                    }
                    else heuristica = valorPecesMeves + valorBuides;
                }

                // Nomes ens quedem amb l'heuristica del millor bloc
                if(heuristicaMillor < heuristica) heuristicaMillor = heuristica;
            }

            return heuristicaMillor;
        }

        /**
         * Calcula l'heuristica de les diagonals i retorna la de la millor,
         * delegant tasques a altres metodes.
         *
         * @param columna Columna de la tirada actual.
         * @param fila Fila de la tirada actual.
         * @return Heuristica de la millor diagonal.
         */
        private int heuristicaDiagonal(int columna, int fila){
            // Generem el bloc per la diagonal ascendent
            Bloc diagonalAscendent = blocDiagonalAscendent(columna, fila);
            // Busquem els blocs que cal analitzar per aquesta diagonal
            int [] blocsPerAnalitzar = triaBlocsDiagonal(diagonalAscendent);
            // I en calculem l'heuristica
            int heuristica = analitzaBlocs(blocsPerAnalitzar, diagonalAscendent);

            // Generem el bloc per la diagonal descendent
            Bloc diagonalDescendent = blocDiagonalDescendent(columna, fila);
            // Busquem els blocs que cal analitzar per aquesta diagonal
            blocsPerAnalitzar = triaBlocsDiagonal(diagonalDescendent);
            // Calculem l'heuristica i la sumem a la que ja portavem
            int heuristicaDescendent = analitzaBlocs(blocsPerAnalitzar, diagonalDescendent);

            // Ens quedem amb la millor
            if(heuristica < heuristicaDescendent) heuristica = heuristicaDescendent;

            // Retornem la suma de les heuristiques de les dues diagonals
            return heuristica;
        }

        /**
         * S'encarrega de triar el conjunt de subblocs que s'han
         * d'analitzar segons la posicio de la tirada actual.
         *
         * @param diagonal Bloc que representa la diagonal sencera.
         * @return Conjunt de subblocs que s'han d'analitzar.
         */
        public int [] triaBlocsDiagonal(Bloc diagonal) {
            // bloc 0: [0..3]
            // bloc 1: [1..4]
            // bloc 2: [2..5]
            // Com es pot veure, fem coincidir el nom del bloc {0,1,2}
            // amb l'index de la primera casella del bloc corresponent
            if(!diagonal.getBloc().isEmpty() && diagonal.getBloc().size() < 4) return new int[]{};
            else {
                // Depenent del tamany de la diagonal i d'on s'hagi tirat,
                // s'hauran d'analitzar uns blocs o uns altres
                int columna = diagonal.getBloc().get(diagonal.getModificada()).getColumna();
                switch(diagonal.getBloc().size()) {
                    case 4:  return new int[]{0};
                    case 5:  if(columna == 0) return new int[]{0};
                             else if(columna == 4) return new int[]{1};
                             else return new int[]{0, 1};
                    case 6:  if(columna == 0) return new int[]{0};
                             else if(columna == 1) return new int[]{0, 1};
                             else if(columna == 4) return new int[]{1, 2};
                             else if(columna == 5) return new int[]{2};
                             else return new int[]{0, 1, 2};
                    default: return new int[]{};
                }
            }
        }

        /**
         * Construeix el Bloc que representa la columna a partir de la posicio
         * modificada.
         *
         * @param columna Columna de la posicio modificada.
         * @param fila Fila de la posicio modificada.
         * @return Bloc que representa la columna.
         */
        private Bloc blocColumna(int columna, int fila){
            Bloc bloc = new Bloc();
            for(int i=0;i<taulerActual.getMaxY();i++) {
                if(i==fila) bloc.setModificada(bloc.getBloc().size());
                bloc.getBloc().add(new Posicio(columna,i));
            }
            return bloc;
        }

        /**
         * Construeix el Bloc que representa la fila a partir de la posicio
         * modificada.
         *
         * @param columna Columna de la posicio modificada.
         * @param fila Fila de la posicio modificada.
         * @return Bloc que representa la fila.
         */
        private Bloc blocFila(int columna, int fila){
            Bloc bloc = new Bloc();
            for(int i=0;i<taulerActual.getMaxX();i++) {
                if(i==columna) bloc.setModificada(bloc.getBloc().size());
                bloc.getBloc().add(new Posicio(i,fila));
            }
            return bloc;
        }

        /**
         * Construeix el Bloc que representa la diagonal ascendent a partir de la posicio
         * modificada.
         *
         * @param columna Columna de la posicio modificada.
         * @param fila Fila de la posicio modificada.
         * @return Bloc que representa la diagonal ascendent.
         */
        private Bloc blocDiagonalAscendent(int columna, int fila){
            Bloc bloc = new Bloc();
            boolean comencament = false;
            int c = columna;
            int f = fila;

            while(!comencament){
                if(c == 0 || f == 0) comencament = true;
                else {
                    c--;
                    f--;
                }
            }

            while(c < 6 && f < 7){
                if(c == columna && f == fila) bloc.setModificada(bloc.getBloc().size());
                bloc.getBloc().add(new Posicio(c, f));
                c++;
                f++;
            }
            return bloc;
        }

        /**
         * Construeix el Bloc que representa la diagonal descendent a partir de la posicio
         * modificada.
         *
         * @param columna Columna de la posicio modificada.
         * @param fila Fila de la posicio modificada.
         * @return Bloc que representa la diagonal descendent.
         */
        private Bloc blocDiagonalDescendent(int columna, int fila){
            Bloc bloc = new Bloc();
            boolean comencament = false;
            int c = columna;
            int f = fila;

            while(!comencament){
                if(c == 5 || f == 0) comencament = true;
                else {
                    c++;
                    f--;
                }
            }

            while(c >= 0 && f < 7){
                if(c == columna && f == fila) bloc.setModificada(bloc.getBloc().size());
                bloc.getBloc().add(new Posicio(c, f));
                c--;
                f++;
            }
            return bloc;
        }

        /**
         * Genera els possibles taulers a partir del taulerActual.
         *
         * @return Possibles taulers.
         */
        private ArrayList<TaulerMatriu> generaMoviments() {
            ArrayList<TaulerMatriu> possiblesFills = new ArrayList<TaulerMatriu>();

            // Nomes cal generar taulers "fill" en cas que no s'hagi guanyat amb la tirada actual
            if(!hasGuanyat()) {
                // No hi ha connecta4, cal generar els taulers amb les possibles tirades fetes
                for(int i=0;i<taulerActual.getMaxX();i++) {
                    int j=0; boolean trobat = false;
                    while(j<taulerActual.getMaxY() && !trobat) {
                        if(taulerActual.getPos(i,j) == 0) {
                            //La posicio i,j esta lliure
                            trobat= true;

                            // Copiem el taulerActual
                            TaulerMatriu copia = new TaulerMatriu(taulerActual);

                            // Fem la possible tirada
                            // Si l'actual es el contrari, ens tocara tirar a la seguent
                            // tirada (Player1 = jugador->1)
                            if(!min) copia.setPos(i,j,1);
                                // el contrari...
                            else copia.setPos(i, j, 2);

                            // I l'apuntem
                            copia.setPosicioModificada(new int[]{i, j});

                            // I l'afegim als possibles fills
                            possiblesFills.add(copia);
                        }
                        j++;
                    }
                }
            }
            return possiblesFills;
        }

        /**
         * Comprova si s'ha guanyat amb la jugada actual.
         *
         * @return Cert si s'ha guanyat, altrament fals.
         */
        private boolean hasGuanyat() {
            int columna = taulerActual.getPosicioModificada()[0];
            int fila = taulerActual.getPosicioModificada()[1];
            int color = taulerActual.getPos(columna,fila);

            return (comptaPecesBloc(blocColumna(columna,fila), color) == 4) ||
                    (comptaPecesBloc(blocFila(columna, fila),color) == 4) ||
                    (comptaPecesBloc(blocDiagonalAscendent(columna,fila),color) == 4) ||
                    (comptaPecesBloc(blocDiagonalDescendent(columna,fila),color) == 4);
        }

        /**
         * Compta les peces seguides del color parametre.
         *
         * @param bloc Bloc per analitzar.
         * @param color Color de la peca.
         * @return Maxim nombre de peces seguides dins del bloc.
         */
        private int comptaPecesBloc(Bloc bloc, int color) {
            if (color==0) return 0;

            int i=0; boolean trobat = false; int pecesSeguides = 0; int maxPecesSeguides = 0;

            while(i<bloc.getBloc().size() && !trobat) {
                int columna = bloc.getBloc().get(i).getColumna();
                int fila = bloc.getBloc().get(i).getFila();
                if(taulerActual.getPos(columna, fila) == color) pecesSeguides++;
                else {
                    if(maxPecesSeguides < pecesSeguides) maxPecesSeguides = pecesSeguides;
                    pecesSeguides = 0;
                    trobat = (bloc.getBloc().size()-1-i) == 3;
                }
                i++;
            }
            return maxPecesSeguides;
        }
    }

    /**
     * Transforma el meutaulell rebut pel metode tirada() a una estructura amb la que
     * nosaltres podem treballar
     */
    private class TaulerMatriu {
        /**
         * Dimensio X
         */
        int maxX;
        /**
         * Dimensio Y
         */
        int maxY;
        /**
         * Matriu que representa el tauler
         */
        int [][] taulerM;

        /**
         * Posicio que ha quedat modificada respecte el tauler anterior
         */
        int [] posicioModificada;

        /**
         * Realitza la transformacio de l'objecte Tauler a TaulerMatriu.
         *
         * @param meuTaulell Tauler grafic a ser transformat a matriu.
         */
        private TaulerMatriu(Tauler meuTaulell) {
            // Obtenim les dimensions de la matriu
            maxX = (int)meuTaulell.getX();
            maxY = (int)meuTaulell.getY();

            // Crear la matriu de dimensions com la del tauler grafic
            taulerM = new int[maxX][maxY];

            //Recorregut del tauler grafic i copia-ne del contingut al tauler matriu
            for(int i=0;i<meuTaulell.getX();i++){
                for(int j=0;j<meuTaulell.getY();j++){
                    taulerM[i][j] = meuTaulell.getpos(i,j);
                }
            }

            //Inicialitzar la posicio modificada
            posicioModificada = new int[]{0,0};
        }

        /**
         * Constructor copia.
         *
         * @param taulerMatriu TaulerMatriu a ser copiat.
         */
        private TaulerMatriu(TaulerMatriu taulerMatriu) {
            // Obtenim les dimensions de la matriu
            maxX = taulerMatriu.maxX;
            maxY = taulerMatriu.maxY;

            // Crear la matriu de dimensions com la del tauler matriu parametre
            taulerM = new int[maxX][maxY];

            //Recorregut del tauler matriu parametre i copia-ne del contingut al tauler matriu
            for(int i=0;i<taulerMatriu.maxX;i++){
                for(int j=0;j<taulerMatriu.maxY;j++){
                    taulerM[i][j] = taulerMatriu.taulerM[i][j];
                }
            }

            // Copia de la posicio modificada
            posicioModificada = taulerMatriu.posicioModificada;
        }

        /**
         *
         * @return Dimensio X.
         */
        public int getMaxX() {
            return maxX;
        }

        /**
         *
         * @return Dimensio Y.
         */
        public int getMaxY() {
            return maxY;
        }

        /**
         * @param x Fila de la posicio.
         * @param y Columna de la posicio.
         * @return Contingut de la posicio (x,y).
         */
        private int getPos(int x, int y) {
            return taulerM[x][y];
        }

        /**
         * Modifica el contingut d'una posicio amb els parametres rebuts.
         *
         * @param x Fila de la posicio.
         * @param y Columna de la posicio.
         * @param valor Contingut que s'ha de posar a la posicio (x,y).
         */
        private void setPos(int x, int y, int valor) {
            taulerM[x][y] = valor;
        }

        /**
         *
         * @return Posicio modificada del tauler.
         */
        public int[] getPosicioModificada() {
            return posicioModificada;
        }

        /**
         * Modifica la posicioModificada, substituint-la per la parametre
         *
         * @param posicioModificada Dades de la posicio modificada.
         */
        public void setPosicioModificada(int[] posicioModificada) {
            this.posicioModificada = posicioModificada;
        }
    }

    /**
     * Classe per definir posicions dins dels blocs
     */
    private class Posicio {
        /**
         *
         * Columna de la posicio.
         */
        private int columna;
        /**
         * Fila de la posicio.
         */
        private int fila;

        /**
         * Inicialitza els valors amb els parametres rebuts.
         *
         * @param columna Columna de la nova posicio.
         * @param fila Fila de la nova posicio.
         */
        public Posicio(int columna, int fila){
            this.columna = columna;
            this.fila = fila;
        }

        /**
         *
         * @return Columna de la posicio.
         */
        public int getColumna() {
            return columna;
        }

        /**
         *
         * @return Fila de la posicio.
         */
        public int getFila() {
            return fila;
        }
    }

    /**
     * Blocs que representen porcions del tauler en forma de
     * conjunts de Posicions (columnes, files, diagonals)
     */
    private class Bloc {
        /**
         * Contenidor de Posicions.
         */
        private ArrayList<Posicio> bloc;
        /**
         * Posicio de la tirada actual dins el contenidor de Posicions.
         */
        private int modificada;

        /**
         * Inicialitzacions per defecte d'un nou Bloc.
         */
        public Bloc(){
            bloc = new ArrayList<Posicio>();
            modificada = 0;
        }

        /**
         *
         * @return Contenidor de Posicions.
         */
        public ArrayList<Posicio> getBloc() {
            return bloc;
        }

        /**
         *
         * @return Posicio de la Posicio modificada.
         */
        public int getModificada() {
            return modificada;
        }

        /**
         * Canvia la Posicio modificada amb el que indica el parametre.
         * @param modificada Nova Posicio modificada.
         */
        public void setModificada(int modificada) {
            this.modificada = modificada;
        }
    }
}
